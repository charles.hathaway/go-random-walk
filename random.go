package main

/* Run for a specified duration calling random features of the YottaDB simple API.

We fork off N threads, where has a chance of:

- Getting a global
- Setting a global
- Killing a global
- Data'ing a global
- Walking through a global
- Walking through a subscript
- Incrementing a global
- Starting a TP transaction
- Ending a TP transaction

The goal is to run for some time with no panics

*/

import (
	"fmt"
	"lang.yottadb.com/go/yottadb"
	"math/rand"
	"time"
	"sync"
//	"runtime"
)

func Ok() {
	// Noop that means everything is OK
}

func genGlobalName() []string {
	var ret []string
	num_subs := rand.Int() % 10 + 1
	global_id := rand.Int() % 2
	switch global_id {
	case 0:
		ret = append(ret, "^MyGlobal1")
	case 1:
		ret = append(ret, "^MyGlobal2")
	}
	for i := 0; i < num_subs; i++ {
		ret = append(ret, fmt.Sprintf("sub%d", i))
	}
	return ret
}

func run_proc(tptoken uint64) int32 {
	action := rand.Float64() * 100

	//fmt.Printf("Action: %d\n", action)

	if action < 10 {
		// Get a global
		t := genGlobalName()
		varname := t[0]
		subs := t[1:]
		_, err := yottadb.ValE(tptoken, varname, subs)
		//fmt.Printf("Done with ValE")
		// There are some error codes we accept; anything other than that, raise an error
		if err != nil {
			errcode := yottadb.ErrorCode(err)
			switch errcode {
			case yottadb.YDB_ERR_GVUNDEF:
				Ok()
			case yottadb.YDB_ERR_INVSVN:
				Ok()
			case yottadb.YDB_ERR_LVUNDEF:
				Ok()
			default:
				panic(fmt.Sprintf("Unexpected return code (%d) issued! %s", errcode, err))
			}
		}
	} else if action < 20 {// Get a global
		t := genGlobalName()
		varname := t[0]
		subs := t[1:]
		err := yottadb.SetValE(tptoken, "MySecretValue", varname, subs)
		//fmt.Printf("Done with SetValE")
		// There are some error codes we accept; anything other than that, raise an error
		if err != nil {
			errcode := yottadb.ErrorCode(err)
			switch errcode {
			case yottadb.YDB_ERR_GVUNDEF:
				Ok()
			case yottadb.YDB_ERR_INVSVN:
				Ok()
			case yottadb.YDB_ERR_LVUNDEF:
				Ok()
			default:
				panic(fmt.Sprintf("Unexpected return code (%d) issued! %s", errcode, err))
			}
		}
	} else if action < 30 {
		// Get a global
		t := genGlobalName()
		varname := t[0]
		subs := t[1:]
		res, err := yottadb.DataE(tptoken, varname, subs)
		//fmt.Printf("Done with DataE")
		// There are some error codes we accept; anything other than that, raise an error
		if err != nil {
			errcode := yottadb.ErrorCode(err)
			switch errcode {
			case yottadb.YDB_ERR_GVUNDEF:
				Ok()
			case yottadb.YDB_ERR_INVSVN:
				Ok()
			case yottadb.YDB_ERR_LVUNDEF:
				Ok()
			default:
				panic(fmt.Sprintf("Unexpected return code (%d) issued! %s", errcode, err))

			}
		}
		switch res {
		case 0:
			Ok()
		case 1:
			Ok()
		case 10:
			Ok()
		case 11:
			Ok()
		default:
			panic("Unexpected data value issued!")
		}
	} else if action < 40 {
		t := genGlobalName()
		varname := t[0]
		subs := t[1:]
		err := yottadb.DeleteE(tptoken, yottadb.YDB_DEL_TREE, varname, subs)
		//fmt.Printf("Done with DeleteE")
		// There are some error codes we accept; anything other than that, raise an error
		if err != nil {
			panic(err)
		}
	} else if action < 50 {
		t := genGlobalName()
		varname := t[0]
		subs := t[1:]
		/*// Pick a random direction
		direction := rand.Float64()
		if direction < .5 {
			direction = 1
		} else {
			direction = -1
		}*/
		retcode := 0
		// TODO: is the documentation for the SIMPLE API still listing as YDB_NODE_END
		for retcode != yottadb.YDB_ERR_NODEEND {
			t, err := yottadb.NodeNextE(tptoken, varname, subs)
			//fmt.Printf("Done with NodeNextE")
			retcode = yottadb.ErrorCode(err)
			subs = t
		}
		if retcode != yottadb.YDB_ERR_NODEEND {
			panic(fmt.Sprintf("Unexpected return code (%d) issued!", retcode))
		}
	} else if action < 60 {
		t := genGlobalName()
		varname := t[0]
		subs := t[1:]
		/*// Pick a random direction
		direction := rand.Float64()
		if direction < .5 {
			direction = 1
		} else {
			direction = -1
		}*/
		retcode := 0
		// TODO: is the documentation for the SIMPLE API still listing as YDB_NODE_END
		for retcode != yottadb.YDB_ERR_NODEEND {
			t, err := yottadb.SubNextE(tptoken, varname, subs)
			//fmt.Printf("Done with SubNextE")
			retcode = yottadb.ErrorCode(err)
			subs[len(subs)-1] = t
		}
		if retcode != yottadb.YDB_ERR_NODEEND {
			panic(fmt.Sprintf("Unexpected return code (%d) issued!", retcode))
		}
	} else if action < 95 {
		t := genGlobalName()
		incr_amount := rand.Float64() * 10 - 5
		varname := t[0]
		subs := t[1:]
		_, err := yottadb.IncrE(tptoken, fmt.Sprintf("%f", incr_amount), varname, subs)
		//fmt.Printf("Done with IncrE")
		// There are some error codes we accept; anything other than that, raise an error
		if err != nil {
			panic(err)
		}
	} else if action < 100 {
		yottadb.TpE2(tptoken, func(tptoken uint64) int32 {
			var wg sync.WaitGroup
			//fmt.Printf("TpToken: %d\n", tptoken)
			for i := 0; i < 10; i++ {
				wg.Add(1)
				go func() {
					run_proc(tptoken)
					wg.Done()
				}()
				run_proc(tptoken)
			}
			wg.Wait()
			return 0
		}, "BATCH", []string{})
		//fmt.Printf("Done with TpE2")
	} else {
		panic("Huh, random number out of range")
	}
	return 0
}

func main() {
	var wg sync.WaitGroup
	
	tptoken := yottadb.NOTTP
	threads := 10
	delay := "60s"
	done := false

	fmt.Printf("Kicking off processes...\n")

	for i := 0; i < threads; i++ {
		wg.Add(1)
		go func() {
			for !done {
				run_proc(yottadb.NOTTP)
				//runtime.GC()
			}
			wg.Done()
		}()
	}

	fmt.Printf("Sleeping for %s...\n", delay)
	t, _ := time.ParseDuration(delay)
	time.Sleep(t)

	done = true
	fmt.Printf("Waiting for procs to stop...\n")
	wg.Wait()

	fmt.Printf("Here! Tptoken is %d\n", tptoken)
}
